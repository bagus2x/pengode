'use client' // Error components must be Client Components

import { errorMessages } from '@pengode/common/axios'
import { useEffect } from 'react'

export default function ErrorPage({
  error,
  reset,
}: {
  error: Error & { digest?: string }
  reset: () => void
}) {
  const messages = errorMessages(error)

  useEffect(() => {
    console.error(error)
  }, [error])

  return (
    <main className='flex h-screen w-screen flex-col items-center justify-center'>
      <h2 className='mb-4'>Something went wrong!</h2>
      <section className='flex flex-col'>
        {messages.map((message, index) => (
          <p key={index}>{message}</p>
        ))}
      </section>
    </main>
  )
}
